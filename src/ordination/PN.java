package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> datoerGivet;
    public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder)
	{
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
		this.datoerGivet = new ArrayList<>();
	}

	

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
     	if(givesDen.isAfter(startDen) && givesDen.isBefore(slutDen)){
     		datoerGivet.add(givesDen);
     		return true;
     	}
     	
        return false;   
    }
    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
   
        return datoerGivet.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

	@Override
	public String getType()
	{
		return "Pro necesare ordination";
	}

	@Override
	public double samletDosis()
	{
		return datoerGivet.size()*antalEnheder;
	}


	@Override
	public double doegnDosis()
	{
		if(datoerGivet.size()>0){
			return samletDosis()/antalDage();
		}
		return 0;
	}

}
