package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalTime[] klokkeSlet, double[] antalEnheder, LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel){
		super(startDen, slutDen, laegemiddel);
		for (int i = 0; i < antalEnheder.length; i++) {
			opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		
	}

    public void opretDosis(LocalTime tid, double antal) {
       	Dosis d = new Dosis(tid, antal);
       	doser.add(d); 
    }

    public ArrayList<Dosis> getDoser(){
    	return doser;
    }
    
  
	@Override
	public double samletDosis()
	{
		return doegnDosis()*antalDage();
	}

	@Override
	public double doegnDosis()
	{
		
		double sum = 0;
		for(Dosis d : doser){
			sum+= d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType()
	{
		return "Daglig skæv ordination";
	}
}
