package ordination;

import java.time.*;

public class DagligFast extends Ordination {
    // TODO
	private Dosis[] doser;


	
	public DagligFast(double morgenAntal, double middagAntal, double aftenAntal, double natAntal, LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel){
		super(startDen, slutDen, laegemiddel);
		this.doser = new Dosis[4];
		doser[0] = new Dosis(LocalTime.of(7, 00), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(23, 30), natAntal);
		
	}
	
	
	public Dosis[] getDoser(){
		return doser;
	}	
	
	@Override
	public double samletDosis()
	{
		return doegnDosis()*antalDage();
	}

	@Override
	public double doegnDosis()
	{
		double sum = 0;
		for(Dosis d : doser){
			sum+= d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType()
	{
		return "Daglig fast ordination";
	}
	
}
